#!/usr/bin/env ruby

require 'gtk3'
require_relative 'src/ui/tracegen_app'

res = Gio::Resource.load('resources/gresources.gresource')

Gio::Resources.register res

pregens = Dir['resources/tracemap/_pregen/*.gpx'].map(&File.method(:realpath))

app = TracegenApp.new(pregens)

app.run