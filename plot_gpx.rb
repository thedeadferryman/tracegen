require 'hashie/mash'
require 'gruff'
require 'tty-progressbar'
require 'date'

require_relative 'src/elevation/elevation_tile_service'
require_relative 'src/track_processor/track_processor'
require_relative 'src/gpx_writer'

def load_gpx(file)
  gpx = Nokogiri::XML(File.open(file))

  track = []

  bar = TrackProcessor.progress 'gpx read'
  bar.log "Selected #{File.basename(file)}"

  prev_trkpt = nil

  bar.iterate(gpx.css('trk trkseg trkpt').each) do |elem|
    # break if track.size > 540

    lat = elem.attributes['lat'].value.to_f
    lon = elem.attributes['lon'].value.to_f
    ele = elem.css('ele').first.text.to_f
    hr = elem.css('extensions')
      &.children
      &.find { |c| c&.name == 'TrackPointExtension' }
      &.children
      &.find { |c| c&.name == 'hr' }
      &.text&.to_f
    cad = elem.css('extensions')
      &.children
      &.find { |c| c&.name == 'TrackPointExtension' }
      &.children
      &.find { |c| c&.name == 'cad' }
      &.text&.to_f

    time = DateTime.parse(elem.css('time').first).to_time.to_i
    vel = 0
    tdiff = 0
    dst = 0

    unless prev_trkpt.nil?
      dst = TrackProcessor.cart_distance({ lat: lat, lon: lon }, prev_trkpt).to_f
      tdiff = (prev_trkpt.time - time).abs

      vel = (dst / tdiff).abs
    end

    prev_trkpt = Hashie::Mash.new({ lat: lat, lon: lon, ele: ele, tdiff: tdiff, dst: dst, time: time, vel: vel, hr: hr, cad: cad || 0 })
    track << prev_trkpt
  end

  track
end

ifile = ARGV.shift || 'generated.gpx'
track = load_gpx(ifile)

track.shift

sm = SignalSmoother.new(:surlinpow, 6, 1.3)

vels = sm.process(track.map(&:vel).map { |v| v })
cads = sm.process(track.map(&:cad))

gr = Gruff::Line.new
gr.data 'vel', vels
# gr.data 'cads', cads
# gr.data 'ele', track.map(&:ele)
# gr.data 'hr', track.map(&:hr).compact

gr.to_image

pp 'writing'

ofile = File.basename(ifile, File.extname(ifile)) + '-plot.png'

gr.write ofile