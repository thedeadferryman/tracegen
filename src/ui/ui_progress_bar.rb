class UiProgressBar
  def initialize(pb, name, total = nil)
    @pb = pb
    @name = name
    @total = total
    @current = 0
  end

  def current=(value)
    @current = value
    set(@current, @total)
  end

  def update(total: nil)
    @total = total unless total.nil?
    set(@current, @total)
  end

  def advance(by, total = nil)
    @current += by
    @total = total unless total.nil?
    set(@current, @total || 1)
  end

  def log(*_) = puts(*_)

  # @param [Enumerator] enum
  def iterate(enum, &blk)
    @current = 0
    total = @total || enum.count
    
    set(@current, total)

    en = Enumerator.new do |iter|
      enum.each do |elem|
        @current += 1
        set(@current, total)
        iter.yield(elem)
      end
    end

    block_given? ? en.each(&blk) : en
  end

  private

  def set(cur, total)
    @pb.fraction = cur.to_f / total
    @pb.text = @name
  end
end