require 'gtk3'
require 'duplicate'

require_relative 'config_model'
require_relative 'model_runner'

class MainWindow < Gtk::ApplicationWindow
  type_register
  CHILDREN = %w[tst_date tst_hour tst_min tst_sec ifile cn_ampl cn_mux avs_min avs_max tm_rescale shr_min shr_max hrc_min hrc_max prc npx dpx x0px1 sp xpx1 run_btn progress sp_alpha]

  class << self
    def init
      set_template resource: '/io/thedeadferryman/tracegen/ui/main.ui'

      CHILDREN.each { |v| bind_template_child v }
    end
  end

  attr_reader :model

  def initialize(application, pregens)
    #noinspection RubyArgCount
    super({ application: application })

    @model = ConfigModel.new
    @pregens = pregens

    set_title 'TraceGen'

    init_form

    run_btn.signal_connect(:clicked) do
      if choose_outfile
        update_model
        run_model
      end
    end
  end

  private

  def run_model
    Thread.new do
      run_btn.sensitive = false
      ModelRunner.new(@model, progress).run
      init_progress 'success'
    rescue Exception => e
      pp e
      pp e.backtrace
      init_progress 'failed'
    ensure
      run_btn.sensitive = true
    end
  end

  def choose_outfile
    dialog = Gtk::FileChooserDialog.new({
                                          title: "Save as...",
                                          parent: self,
                                          action: :save,
                                          buttons: [
                                            ['Select', :ok],
                                            ['Cancel', :cancel],
                                          ]
                                        })

    if dialog.run == :ok
      dialog.close

      update_io dialog.filename
      true
    else
      dialog.close
      false
    end
  end

  def init_form
    init_start_time
    init_ifile
    init_progress
  end

  def init_progress(msg = 'idle')
    UiProgressBar.new(progress, msg, 1).current = 0
  end

  def init_start_time
    time = duplicate(@model.time)
    date = duplicate(@model.date)

    tst_date.day = date[2]
    tst_date.month = date[1] - 1
    tst_date.year = date[0]
    tst_sec.value = time[2]
    tst_min.value = time[1]
    tst_hour.value = time[0]
  end

  def init_ifile
    # @pregens.each do |ifl|
    #   ifile.append ifl, File.basename(ifl)
    # end
    #
    # ifile.active_id = @pregens.sample
  end

  def update_xform_params
    @model.cart_noise = [cn_ampl, cn_mux].map(&:text).map(&:to_f)
    @model.avg_speed = [avs_min, avs_max]
      .map(&:text)
      .map(&:to_f)
      .each_slice(2)
      .map { |v| v[0]..v[1] }
    @model.speed_alpha = sp_alpha.text.to_f
  end

  def update_start_time
    @model.rescale = tm_rescale.text.to_f
    @model.date = tst_date.date
    @model.time = [tst_hour, tst_min, tst_sec].map(&:adjustment).map(&:value)
  end

  def update_ode_params
    @model.odeparams = [prc, npx, dpx, x0px1, sp, xpx1].map(&:text).map(&:to_f)
  end

  def update_hr_params
    @model.hrparams = [shr_min, shr_max, hrc_min, hrc_max]
      .map(&:text)
      .map(&:to_f)
      .each_slice(2)
      .map { |v| v[0]..v[1] }
  end

  def update_io(ofile)
    @model.ifile = ifile.filename
    @model.ofile = ofile
  end

  def update_model
    update_ode_params
    update_start_time
    update_hr_params
    update_xform_params

    pp @model
  end

  def bind(signals, &blk)
    signals.each do |sig|
      target, signal = sig

      target.signal_connect(signal, &blk)
    end
  end
end