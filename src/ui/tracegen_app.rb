require 'gtk3'

require_relative 'main_window'

class TracegenApp < Gtk::Application
  def initialize(pregens)
    super 'io.thedeadferryman.tracegen', Gio::ApplicationFlags::FLAGS_NONE

    signal_connect :activate do |app|
      window = MainWindow.new(app, pregens)

      window.present
    end
  end
end