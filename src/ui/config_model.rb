require 'date'

class ConfigModel
  PROPERTIES = %i[date time odeparams hrparams rescale cart_noise avg_speed ifile ofile speed_alpha]

  attr_accessor *PROPERTIES

  def initialize
    now = DateTime.now
    @date = [now.year, now.month, now.mday]
    @time = [now.hour, now.minute, now.second]
  end

  def start_time
    DateTime.new(*@date, *@time).to_time
  end
end