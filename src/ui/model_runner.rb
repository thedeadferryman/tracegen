require_relative 'ui_progress_bar'
require_relative '../track_processor/track_processor'
require_relative '../elevation/elevation_tile_service'
require_relative '../gpx_writer'
require_relative '../util/noise_profile'
require_relative '../util/signal_scaler'

class ModelRunner
	class << self
		def load_gpx(file)
			gpx = Nokogiri::XML(File.open(file))

			track = []

			bar = TrackProcessor.progress 'gpx read'
			bar.log "Selected #{File.basename(file)}"

			bar.iterate(gpx.css('trk trkseg trkpt').each) do |elem|
				lat = elem.attributes['lat'].value.to_f
				lon = elem.attributes['lon'].value.to_f

				track << Hashie::Mash.new({ lat: lat, lon: lon })
			end

			track
		end

	end

	# @param [ConfigModel] config
	# @param [Object] pb
	def initialize(config, pb)
		@config = config
		@pb     = pb
	end

	def run
		srand(Time.now.to_i)

		TrackProcessor.progress_factory = ->(name, total = nil) do
			UiProgressBar.new(@pb, name, total)
		end

		ets = ElevationTileService.new(token: 'pk.eyJ1Ijoia21laW5rb3BmIiwiYSI6ImNrcnhobGg4NzBxZ3YydnFuemg2d2dmMTEifQ.fuIlKwPPbL5_DaKWVzY-Vw')

		speed_params = @config.avg_speed.map(&method(:rand))

		tpu = TrackProcessor.new
		                    .with_cart_noise(*@config.cart_noise)
		                    .with_elevation(ets)
		                    .fold
		                    .with_metrics
		                    .with_avg_speed(*speed_params, @config.speed_alpha)
		                    .with_elapsed
		                    .rescale(@config.rescale)
		                    .unfold
		                    .with_elevation(ets)
		                    .fold
		                    .with_metrics
		                    .with_avg_speed(*speed_params, @config.speed_alpha)
		                    .with_noise_profile(:speed, NoiseProfile::Gaussian.new(0.1, SignalScaler.new(320)))
		                    .with_noise_profile(:speed, NoiseProfile::Gaussian.new(0.8, SignalScaler.new(1)))
		                    .with_elapsed
		                    .rescale(@config.rescale)
		                    .unfold
		                    .with_elevation(ets)
		                    .fold
		                    .with_metrics
		                    .unfold(@config.start_time.to_i)
		                    .with_heartbeat_ode(*@config.hrparams.map(&method(:rand)), *@config.odeparams)
		                    .with_noise_profile(:hr, NoiseProfile::Gaussian.new(5, SignalScaler.new(64)))
		                    .with_noise_profile(:hr, NoiseProfile::Gaussian.new(5, SignalScaler.new(8)))
		                    .with_noise_profile(:hr, NoiseProfile.new(
		                      256..384, 4..8, -8..8, 1,
		                      SignalSmoother.new(:surlinpow, 2, 1.2)
		                    ))
		                    .with_baseline(:cad, rand(48..55))
		                    .with_noise_profile(:cad, NoiseProfile::Gaussian.new(2, SignalScaler.new(36)))
		                    .with_noise_profile(:cad, NoiseProfile::Gaussian.new(1, SignalScaler.new(4)))
		                    .with_noise_profile(:cad, NoiseProfile.new(
		                      480..960, 12..18, 5..8, 0.5,
		                      SignalSmoother.new(:surlinpow, 32, 1.2)
		                    ))
		                    .with_noise_profile(:cad, NoiseProfile.new(
		                      1750..2700, 8..12, 8..12, 0.5,
		                      SignalSmoother.new(:surlinpow, 16, 1.2)
		                    ))
		                    

		track = ModelRunner.load_gpx @config.ifile

		segs    = tpu.process track
		outfile = File.open(@config.ofile, 'w')
		
        pp ['avg hr', segs.map(&:hr).sum / segs.size]
		
		wobar = TrackProcessor.progress('gpx write')

		wobar.log "Writing #{@config.ofile}"

		GpxWriter.new(segs, 'test').write_gpx(outfile, wobar)

		outfile.close
	end
end
