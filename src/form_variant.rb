# frozen_string_literal: true

require 'nokogiri'

# c;a
class VariantFormer
	def initialize(preset)
		@preset = preset
	end

	def form(var)
		var.map(&method(:parse_fragment)).flatten(1)
	end

	private

	def parse_fragment(frag)
		fpath = File.realpath("resources/tracemap/#{@preset}/#{frag}.gpx")

		doc = File.open(fpath) { |f| Nokogiri::XML(f) }

		doc.css('trk > trkseg > trkpt').map do |trkpt|
			{
				lon: (trkpt.attr('lon').to_f),
				lat: (trkpt.attr('lat').to_f),
				ele: trkpt.css('ele').first.children.to_s.to_f
			}
		end.to_a
	end
end
