# frozen_string_literal: true

require 'fiddle'

module LibODEInt
	DYLIBEXTS = ['.dll', '.so', '.dylib', ''].freeze
	DYLIBNAMES = %w[libodeint odeint].freeze
	DYLIBPATHS = ENV['PATH'].split(File::PATH_SEPARATOR) + %w[. ./ext/]

	def self.ldlopen_any
		libfiles = DYLIBNAMES
			.map { |f| DYLIBEXTS.map { |e| [f, e].join } }.flatten
			.map { |f| DYLIBPATHS.map { |p| File.join(p, f) } }.flatten

		libfiles.each do |lib|
			return Fiddle.dlopen(lib)
		rescue StandardError
			nil
		end

		raise "Cannot load any of dylibs: #{libfiles.join(', ')}"
	end

	DYLIB = LibODEInt.ldlopen_any

	class StType
		include Enumerable

		attr_reader :ptr

		def initialize
			@ptr = ffi_new.call
		end

		def each(&blk)
			(0...size).each do |i|
				blk.call(self[i])
			end
		end

		def <<(value)
			ffi_add.call(@ptr, value)
		end

		def [](index)
			ffi_at.call(@ptr, index)
		end

		def size
			ffi_size.call(@ptr)
		end

		def drop
			ffi_drop.call(@ptr)
		end

		private

		def ffi_new
			@ffi_new ||= Fiddle::Function.new(
				DYLIB['StTypeNew'],
				[],
				Fiddle::TYPE_VOIDP
			)
		end

		def ffi_add
			@ffi_add ||= Fiddle::Function.new(
				DYLIB['StTypeAdd'],
				[Fiddle::TYPE_VOIDP, Fiddle::TYPE_DOUBLE],
				Fiddle::TYPE_VOID
			)
		end

		def ffi_at
			@ffi_at ||= Fiddle::Function.new(
				DYLIB['StTypeAt'],
				[Fiddle::TYPE_VOIDP, Fiddle::TYPE_SIZE_T],
				Fiddle::TYPE_DOUBLE
			)
		end

		def ffi_size
			@ffi_size ||= Fiddle::Function.new(
				DYLIB['StTypeSize'],
				[Fiddle::TYPE_VOIDP],
				Fiddle::TYPE_SIZE_T
			)
		end

		def ffi_drop
			@ffi_drop ||= Fiddle::Function.new(
				DYLIB['StTypeDrop'],
				[Fiddle::TYPE_VOIDP],
				Fiddle::TYPE_VOID
			)
		end
	end

	class Integrator
		def initialize(speeds, step_size, npx = 2.31e-5, dpx = 15.81, x0px1 = 36.12, sp = 0.42, xpx1 = 2.08e-4) # rubocop:disable Metrics/ParameterLists, Naming/MethodParameterName
			@ptr = ffi_new.call(speeds.ptr, step_size, npx, dpx, x0px1, sp, xpx1)
		end

		def precompute(tmax, &blk)
			closure = block_to_closure blk

			fn = Fiddle::Function.new(
				closure,
				[Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE],
				Fiddle::TYPE_VOID
			)

			ffi_precompute.call(@ptr, tmax, fn)
		end

		def call(time)
			ffi_call.call(@ptr, time)
		end

		def drop
			ffi_drop.call(@ptr)
		end

		private

		def block_to_closure(blk)
			Class.new(Fiddle::Closure) do
				def initialize(blk, *args)
					@blk = blk || ->(*_) {}
					super(*args)
				end

				def call(t_v, x_v) = @blk.call(t_v, x_v)
			end.new(
				blk, Fiddle::TYPE_VOID,
				[Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE]
			)
		end

		def ffi_new
			@ffi_new ||= Fiddle::Function.new(
				DYLIB['IntegratorNew'],
				[
					Fiddle::TYPE_VOIDP,
					Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE,
					Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE
				],
				Fiddle::TYPE_VOIDP
			)
		end

		def ffi_precompute
			@ffi_precompute ||= Fiddle::Function.new(
				DYLIB['IntegratorPrecompute'],
				[Fiddle::TYPE_VOIDP, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_VOIDP],
				Fiddle::TYPE_VOID
			)
		end

		def ffi_call
			@ffi_call ||= Fiddle::Function.new(
				DYLIB['IntegratorCall'],
				[Fiddle::TYPE_VOIDP, Fiddle::TYPE_DOUBLE],
				Fiddle::TYPE_DOUBLE
			)
		end

		def ffi_drop
			@ffi_drop ||= Fiddle::Function.new(
				DYLIB['IntegratorDrop'],
				[Fiddle::TYPE_VOIDP],
				Fiddle::TYPE_VOID
			)
		end
	end
end
