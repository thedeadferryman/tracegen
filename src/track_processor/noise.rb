require 'hashie/mash'

module Noise
  # @param [Symbol] param
  # @param [NoiseProfile] profile
  def with_noise_profile(track, param, profile)
    noise = profile.produce(track.size).first(track.size)

    progress('noise add').iterate(track.zip(noise).map) do |it|
      Hashie::Mash.new({
        **it[0],
        param => it[0][param] + it[1]
      })
    end
  end
end