module Folding
	def fold(track)
		last_trkpt = nil
		segs       = []

		progress('fold group').iterate(track.map) { |v| Hashie::Mash.new(v) }.each do |trkpt|
			segs << [last_trkpt, trkpt] unless last_trkpt.nil?

			last_trkpt = trkpt
		end

		progress('fold fmt').iterate(segs.map) do |seg|
			Hashie::Mash.new({
			                   start: seg[0],
			                   fin:   seg[1]
			                 })
		end
	end

	def unfold(folded_track, stop_time = 0, alpha = 0.45)
		trkpts     = []
		start_time = cur_ts = stop_time - folded_track.map { |v| v.time || 0 }.sum
		cur_dist   = 0

		progress('unfold').iterate(folded_track.each) do |seg|
			trkpt        = duplicate(seg.start)
			trkpt.time   = cur_ts
			trkpt.tsdiff = cur_ts - start_time
			trkpt.dist   = cur_dist
			trkpts << trkpt

			cur_ts   += seg.elapsed || 0
			cur_dist += seg.distance || 0
		end

		last_trkpt        = duplicate(folded_track.last.fin)
		last_trkpt.time   = cur_ts
		last_trkpt.tsdiff = cur_ts - stop_time
		last_trkpt.dist   = cur_dist

		trkpts + [last_trkpt]
	end
end