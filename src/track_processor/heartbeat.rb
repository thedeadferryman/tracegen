require 'duplicate'
require 'hashie/mash'
require 'open3'
require 'json'
require 'csv'
require 'tty/progressbar'

require_relative '../util/signal_smoother'
require_relative '../util/pb_hack'
require_relative '../lib_ode_int'

module Heartbeat
  def with_heartbeat_ode(track, start_rate = 95, coef = 5, precision = 1,
                         npx = 2.31e-5, dpx = 15.81, x0px1 = 36.12, sp = 0.42, xpx1 = 2.08e-4)
    speeds = []

    prev_trkpt = track.shift

    tdiff = track.first.time - prev_trkpt.time
    dsdiff = track.first.dist - prev_trkpt.dist

    vel0 = dsdiff / tdiff

    pp vel0

    progress('pulserODE speed est').iterate(track.each) do |trkpt|
      vel = if prev_trkpt.nil?
        0
      else
        tdiff = trkpt.time - prev_trkpt.time
        dsdiff = trkpt.dist - prev_trkpt.dist

        ((dsdiff / tdiff)) * (1 + noise(0.2))
      end

      prev_trkpt = trkpt

      speeds << vel
    end


    speeds_st = LibODEInt::StType.new

    pp [speeds.sum, speeds.size, speeds.sum / speeds.size]

    progress('pulserODE load').iterate(speeds.each) { speeds_st << speeds.sum / speeds.size }

    tmax = track.map(&:tsdiff).max

    pp tmax

    integ = LibODEInt::Integrator.new(speeds_st, precision / 60.0, npx, dpx, x0px1, sp, xpx1)

    pbar = progress('pulserODE precomp', total: tmax)

    integ.precompute(tmax + 1) { |t| pbar.current = t }

    prg = progress('pulserODE comp', total: tmax)

    hrs = prg.iterate(track.map) do |trkpt|
      x0 = integ.call(trkpt.tsdiff)

      (x0 * coef + start_rate)
    end

    track.zip(hrs).map do |it|
      trkpt, hr = it

      Hashie::Mash.new({
                         **trkpt,
                         hr: hr.round
                       })
    end
  end

  def noise(ampl)
    rand((-ampl)..ampl)
  end
end