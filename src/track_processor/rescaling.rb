module Rescaling
  def rescale(folded_track, time_scale = 60)
    o_segs = duplicate(folded_track)
    segs = []

    pbar = progress('rescale', total: folded_track.length)

    cur_seg = o_segs.shift

    loop do
      pbar.current = folded_track.length - o_segs.length
      if (cur_seg.elapsed - time_scale).abs <= 0
        segs << cur_seg
        break if o_segs.empty?
        cur_seg = o_segs.shift
      elsif cur_seg.elapsed > time_scale
        sg, rest = interpolate_at(cur_seg, time_scale)
        segs << sg
        cur_seg = rest
      elsif cur_seg.elapsed < time_scale
        break if o_segs.empty?

        cur_seg = join_segs(cur_seg, o_segs.shift)
      end
    end

    progress('rescale detach').iterate(segs.map) { |seg| duplicate(seg) }
  end

  private

  def interpolate_at(segment, time_scale)
    frac = time_scale / segment.elapsed

    sgs = duplicate(segment.start)
    restf = duplicate(segment.fin)

    intp = interpolate_coords(sgs, restf, frac)

    sg = Hashie::Mash.new({
                            start: sgs,
                            fin: intp,
                            elapsed: time_scale
                          })

    rest = Hashie::Mash.new({
                              start: intp,
                              fin: restf,
                              elapsed: segment.elapsed - time_scale
                            })

    [sg, rest]
  end

  def interpolate_coords(start, fin, frac)
    ang = cart_distance(start, fin) / Metrics::EARTH_RADIUS

    a = Math.sin((1.0 - frac) * ang) / Math.sin(ang)
    b = Math.sin(frac * ang) / Math.sin(ang)

    phi1 = start.lat * Math::PI / 180
    phi2 = fin.lat * Math::PI / 180
    lam1 = start.lon * Math::PI / 180
    lam2 = fin.lon * Math::PI / 180

    x = a * Math.cos(phi1) * Math.cos(lam1) + b * Math.cos(phi2) * Math.cos(lam2)
    y = a * Math.cos(phi1) * Math.sin(lam1) + b * Math.cos(phi2) * Math.sin(lam2)
    z = a * Math.sin(phi1) + b * Math.sin(phi2)

    phi_i = Math.atan2(z, Math.sqrt(x ** 2 + y ** 2))
    lam_i = Math.atan2(y, x)

    Hashie::Mash.new({
                       lat: phi_i * 180 / Math::PI,
                       lon: lam_i * 180 / Math::PI,
                     })
  end

  def join_segs(seg1, seg2)
    dst1 = cart_distance(seg1.start, seg1.fin)
    dst2 = cart_distance(seg2.start, seg2.fin)
    dst = cart_distance(seg1.start, seg2.fin)
    vp = [dst1, dst2].sum / [seg1.elapsed, seg2.elapsed].sum
    elapsed = (dst / vp)

    Hashie::Mash.new({
                       start: duplicate(seg1.start),
                       fin: duplicate(seg2.fin),
                       elapsed: elapsed
                     })
  end

  def fit(x, min, max)
    [[x, min].max, max].min
  end
end