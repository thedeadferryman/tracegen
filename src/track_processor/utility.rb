module Utility
  SPEED_MUX = 100_000
  NOISE_MUX = 960_000

  def with_baseline(track, param, value)
    track.map do |elem|
      Hashie::Mash.new({
                         **elem,
                         param => value
                       })
    end
  end

  def with_smoother(track, param, smoother)
    val = smoother.process(track.map { |v| v[param] })

    track.zip(val).map do |it|
      elem, value = it

      Hashie::Mash.new({
                         **elem,
                         param => value
                       })
    end
  end

  def touch(value, blk)
    value.tap { |e| blk.call(e) }
  end

  def progress_parent=(bar)
    @parent_bar = bar
  end

  def progress(name, total: nil)
    progress_factory.call(name, total)
  end

  def progress_factory=(pf)
    @progress_factory = pf
  end

  private

  def progress_factory
    @progress_factory ||= ->(name, total) do
      max_len = 20
      nam = name + ' ' * [max_len - name.length, 0].max

      if @parent_bar.nil?
        TTY::ProgressBar.new(
          "#{nam} :bar  (:current/:total) :percent ETA: :eta",
          bar_format: :blade,
          width: TTY::Screen.width * 2 / 3,
          total: total
        )
      else
        @parent_bar.register(
          "#{nam} (:current/:total)  :bar  :percent ETA: :eta",
          bar_format: :blade,
          width: TTY::Screen.width * 2 / 3,
          total: total
        )

      end
    end
  end
end