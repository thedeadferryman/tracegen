# frozen_string_literal: true

require 'hashie/mash'
require 'duplicate'
require_relative '../elevation/elevation_tile_service'

Dir["#{File.dirname(__FILE__)}/*.rb"]
  .map(&File.method(:realpath))
  .reject { |f| f == File.realpath(__FILE__) }
  .each { |f| require f }

class TrackProcessor
  #noinspection RubyResolve
  class << self
    [AvgSpeed, Folding, Heartbeat, Metrics, Noise, Rescaling, Utility].each(&method(:include))
  end

  def initialize(pipe = [])
    @pipe = pipe.map(&method(:pipe_stage)).compact
  end

  def process(track)
    result = track

    @pipe.each do |meth|
      result = meth.call(result)
    end

    result
  end

  def method_missing(symbol, *args)
    @pipe << pipe_stage([symbol, *args])

    self
  end

  def respond_to_missing?(*args)
    TrackProcessor.respond_to?(*args)
  end

  private

  def pipe_stage(stage)
    case stage
    when Symbol
      TrackProcessor.method(stage)
    when Proc
      stage
    when Array
      el = duplicate(stage)
      meth_n = el.shift
      meth = pipe_stage(meth_n)
      ->(stage) { meth.call(stage, *el) }
    else
      ->(stage) { stage }
    end
  end
end
