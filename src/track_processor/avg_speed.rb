module AvgSpeed
  def with_avg_speed(folded_track, avg_speed, alpha = 0.45)
    last_speed = nil

    segs = duplicate(folded_track)

    progress('speed est').iterate(segs.each) do |seg|
      if seg.distance.zero?
        seg.speed = -1
        break
      end

      slope = seg.elediff / seg.distance

      speed = avg_speed * (1 - 0.9 * squash(slope))

      speed = alpha * speed + (1 - alpha) * last_speed unless last_speed.nil?
      last_speed = speed

      seg.speed = speed
    end

    segs
  end

  def norm_speed(folded_track, avg_speed)
    cur_avg = folded_track.map(&:speed).reduce(:+) / folded_track.size
    diff = cur_avg - avg_speed
    progress('speed norm').iterate(folded_track.map) do |seg|
      Hashie::Mash.new({
                         **seg,
                         speed: seg.speed + diff
                       })
    end
  end

  def with_elapsed(folded_track)
    progress('infer time').iterate(folded_track.map) do |seg|
      Hashie::Mash.new({
                         **seg,
                         elapsed: seg.distance / seg.speed
                       })
    end
  end

  private

  def squash(slope)
    sl = slope / 1000

    sl / (1 + sl.abs)
  end
end