module Metrics
  EARTH_RADIUS = 6_371_000.0

  def with_cart_noise(track, ampl = 1.5, scale = NOISE_MUX)
    progress('noise').iterate(track.map) do |trkpt|
      trkpt.tap do |t|
        t.lon = noise_coord(t.lon, ampl, scale)
        t.lat = noise_coord(t.lat, ampl, scale)
      end
    end
  end

  def cart_distance(start, fin)
    phi1 = start[:lat] * Math::PI / 180
    phi2 = fin[:lat] * Math::PI / 180
    d_phi = (fin[:lat] - start[:lat]) * Math::PI / 180
    d_lambda = (fin[:lon] - start[:lon]) * Math::PI / 180

    a = Math.sin(d_phi / 2) * Math.sin(d_phi / 2) +
      Math.cos(phi1) * Math.cos(phi2) *
        Math.sin(d_lambda / 2) * Math.sin(d_lambda / 2)

    2 * EARTH_RADIUS * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  end

  def with_metrics(folded_track)
    progress('metrics').iterate(folded_track.map) do |seg|
      dist = cart_distance(seg.start, seg.fin)
      elediff = (seg.fin.ele) - (seg.start.ele)
      elapsed = nil
      unless seg.fin.time.nil? || seg.start.time.nil?
        elapsed = (seg.fin.time) - (seg.start.time)
      end

      Hashie::Mash.new({
                         **seg,
                         elediff: elediff,
                         distance: dist,
                         elapsed: elapsed,
                       })
    end
  end

  def with_elevation(track, tile_service, alpha = 0.3)
    prev_ele = nil

    trk = duplicate(track)
    progress('elevation').iterate(trk.each) do |trkpt|
      mt = MapTile.from_point(trkpt.lat, trkpt.lon)
      tile = tile_service[mt]

      ele = tile[trkpt.lat, trkpt.lon]

      ele = ele * alpha + prev_ele * (1 - alpha) unless prev_ele.nil?

      prev_ele = trkpt.ele = ele
    end

    trk
  end

  protected

  def noise(ampl)
    rand(-ampl..ampl)
  end

  def noise_coord(coord, ampl, scale)
    coord *= scale

    coord += rand((-ampl)..ampl)

    (coord / scale).ceil(8)
  end

end