# frozen_string_literal: true

require 'faraday'
require 'json'
require 'nokogiri'
require 'fileutils'
require 'oily_png'

require_relative 'map_tile'

class ElevationTileService
  API_BASE = 'https://api.mapbox.com/v4/mapbox.terrain-rgb'
  QUERY_STRING = "#{API_BASE}/%<z>s/%<x>s/%<y>s.pngraw?access_token=%<token>s"

  def initialize(storage_dir = './resources/tiles/', token:)
    begin
      @storage_dir = File.realpath(storage_dir)
    rescue Errno::ENOENT
      FileUtils.mkdir_p storage_dir
      @storage_dir = File.realpath(storage_dir)
    end
    @cache = {}
    @token = token
  end

  def [](tile)
    tile_path = tile_path(tile)

    return tile_with_data(tile, tile_data(tile)) if exists?(tile)

    data = load_tile(tile)

    ensure_path(tile)
    IO.binwrite(tile_path, data)

    @cache[tile.to_id] = ChunkyPNG::Image.from_datastream(
      ChunkyPNG::Datastream.from_blob(data)
    )

    tile_with_data(tile, @cache[tile.to_id])
  end

  private

  def exists?(tile)
    path = tile_path(tile)

    !@cache[tile.to_id].nil? || File.exist?(path)
  end

  def tile_path(tile)
    x, y, z = tile.to_a
    File.join @storage_dir, "#{z}/#{x}/#{y}.png"
  end

  def tile_with_data(tile, data)
    tl = tile.dup
    tl.data = data

    tl
  end

  def tile_data(tile)
    tile_path = tile_path(tile)
    @cache[tile.to_id] ||= ChunkyPNG::Image.from_file(tile_path)
  end

  def load_tile(tile)
    x, y, z = tile.to_a

    query = format(QUERY_STRING, x: x, y: y, z: z, token: @token)

    Faraday.get(query).body
  end

  def ensure_path(tile)
    path = tile_path(tile)

    dirpath = File.dirname path

    FileUtils.mkdir_p dirpath
  end
end


