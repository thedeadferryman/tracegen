# frozen_string_literal: true

require 'oily_png'
require_relative 'map_tile_reader'

class MapTile
	include MapTileReader

	DEG_TO_RAD = Math::PI / 180.0
	RAD_TO_DEG = 180.0 / Math::PI

	attr_accessor :data

	class << self
		def from_point(lat, lon, zoom: 15)
			x, y, z = point_to_tile_frac(lat, lon, zoom)

			MapTile.new(x.floor, y.floor, z)
		end

		private

		def point_to_tile_frac(lat, lon, zoom)
			sin = Math.sin(lat * DEG_TO_RAD)
			z2 = 1 << zoom
			x = z2 * (lon / 360.0 + 0.5)
			y = z2 * (0.5 - 0.25 * Math.log((1 + sin) / (1 - sin)) / Math::PI)

			x = x % z2
			x += z2 if x.negative?

			[x, y, zoom]
		end
	end

	def initialize(x, y, z)
		@t_x = x
		@t_y = y
		@zoom = z
	end

	def lon
		(@t_x * 360.0) / (1 << @zoom) - 180
	end

	def lat
		n = Math::PI - 2 * Math::PI * @t_y / (1 << @zoom)
		RAD_TO_DEG * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n)))
	end

	def bounds
		w = lon
		n = lat
		e = MapTile.new(@t_x + 1, @t_y, @zoom).lon
		s = MapTile.new(@t_x, @t_y + 1, @zoom).lat

		[[w, e].sort, [n, s].sort]
	end

	def to_id
		"#{@t_x}.#{@t_y}@#{@zoom}"
	end

	def to_a
		[@t_x, @t_y, @zoom]
	end

	def dup
		MapTile.new(@t_x, @t_y, @zoom).tap do |tl|
			tl.data = @data unless @data.nil?
		end
	end

	private

	def spans
		lon_b, lat_b = bounds
		[
			(lon_b[1] - lon_b[0]).abs,
			(lat_b[1] - lat_b[0]).abs
		]
	end
end
