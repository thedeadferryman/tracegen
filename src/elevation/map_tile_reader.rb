# frozen_string_literal: true

module MapTileReader
  def [](lat, lon)
    extent = point_extent(lat, lon)

    read_extent extent
  end

  def point_extent(lat, lon)
    x, y = point_precise(lat, lon)

    [
      [x.floor, x.ceil, x].map { |v| fit(v, 0, @data.width - 1) },
      [y.floor, y.ceil, y].map { |v| fit(v, 0, @data.height - 1) }
    ]
  end

  def read_extent(extent)
    xs, ys = extent

    ele00 = read_point(xs[0], ys[0])
    ele01 = read_point(xs[0], ys[1])
    ele10 = read_point(xs[1], ys[0])
    ele11 = read_point(xs[1], ys[1])

    dx = xs[2] - xs[0]
    dy = ys[2] - ys[0]

    (ele00 * (1 - dx) * (1 - dy) +
      ele01 * (1 - dx) * dy +
      ele10 * dx * (1 - dy) +
      ele11 * dx * dy)
  end

  def read_point(x, y)
    if x >= @data.width || y >= @data.height
      return read_point(
        [x, @data.width - 1].min,
        [y, @data.height - 1].min
      )
    end

    var = @data[x, y]
    rgb_to_ele(var)
  end

  def point_precise(lat, lon)
    lon_b, lat_b = bounds
    lon_span, lat_span = spans

    lon_shift = (lon - lon_b[0]).abs
    lat_shift = (lat - lat_b[0]).abs

    [
      (lon_shift * @data.width) / lon_span,
      (lat_shift * @data.height) / lat_span
    ]
  end

  private

  def fit(val, min, max)
    [
      [val, min].max,
      max
    ].min
  end

  def rgb_to_ele(var)
    r = ChunkyPNG::Color.r(var) * (1 << 16)
    g = ChunkyPNG::Color.g(var) * (1 << 8)
    b = ChunkyPNG::Color.b(var)

    (r + g + b) / 10.0 - 10_000
  end
end
