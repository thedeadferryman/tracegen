class SignalScaler
  class << self
    def resize(old_size, new_size)
      SignalScaler.new([new_size, old_size])
    end
  end

  def initialize(scale)
    @scale = case scale
    when Numeric
      [scale, 1]
    when Array
      if scale.take(2).all? { |v| v.is_a? Numeric }
        scale.take(2)
      else
        raise ArgumentError, 'scale array must contain only numeric'
      end
    else
      raise ArgumentError, "scale expected a numeric or array of numeric, got <#{scale.class}>"
    end
  end

  def scale(signal)
    new_size = scale_size signal.size

    Enumerator.new(new_size) do |yielder|
      sgn = signal.to_enum
      signal_data = []

      scale = signal.size / new_size.to_f

      new_size.times do |i|
        loc = i * scale

        until signal_data.size > loc.ceil
          begin
            signal_data << sgn.next
          rescue StopIteration
            break
          end
        end

        alpha = loc - loc.floor

        lower = signal_data[fit(loc.floor.to_i, 0, signal_data.size - 1)]
        upper = signal_data[fit(loc.ceil.to_i, 0, signal_data.size - 1)]

        yielder << (lower * (1 - alpha) + upper * alpha)
      end
    end.lazy
  end

  def scale_size(old_size)
    (old_size.to_f * @scale[0] / @scale[1]).ceil.to_i
  end

  def unscale_size(new_size)
    (new_size.to_f * @scale[1] / @scale[0]).ceil.to_i
  end

  private

  def fit(x, min, max)
    [[x, min].max, max].min
  end
end