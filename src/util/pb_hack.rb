require 'tty/progressbar'

module TTY
	def ProgressBar.max_columns
		TTY::Screen.width - 4
	end
end