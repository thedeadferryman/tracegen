class SignalSmoother
  ALGOS = %i[revalpha surlin2 surlinpow surexplin surexppow movavg]


  def initialize(algo, *args, progress: nil)
    raise 'invalid algo' unless ALGOS.include? algo

    @algo = algo
    @args = args
    @pbar = progress
  end

  def process(enumerator)
    Enumerator.new(enumerator.size) { |yielder| method(@algo).call(yielder, enumerator, *@args) }.lazy
  end

  private

  def surlin2(yielder, enumerator, radius, coef = 1)
    surround(yielder, radius, enumerator) { |idx, pick| radius - coef * (idx - pick).abs }
  end

  def surlinpow(yielder, enumerator, radius, pow)
    surround(yielder, radius, enumerator) { |idx, pick| (radius - (idx - pick).abs) ** pow }
  end

  def surexplin(yielder, enumerator, radius, coef = 1)
    surround(yielder, radius, enumerator) { |idx, pick| 1 - (coef * (idx - pick).abs / radius.to_f) }
  end

  def surexppow(yielder, enumerator, radius, pow)
    surround(yielder, radius, enumerator) { |idx, pick| 1 - ((idx - pick).abs / radius.to_f) ** pow }
  end

  def movavg(yielder, enumerator, radius)
    surround(yielder, radius, enumerator) { 1 }
  end

  def surround(yielder, radius, enumerator, &dif_comp)
    raise 'radius twice greater than signal size' if (radius * 2) >= (enumerator&.size || 0)

    peaks = enumerator.to_enum

    pick = 0
    surrval = []
    radius.times { surrval << peaks.next }

    loop do
      sum, count = surrval
        .map.with_index { |val, idx| [val, dif_comp.call(idx, pick)] }
        .map { |ent| [ent[0] * ent[1], ent[1]] }
        .reduce { |memo, cur| memo.zip(cur).map(&:sum) }

      yielder << sum.to_f / count

      begin
        pick += 1 if pick < (radius - 1)
        surrval << peaks.next
        surrval.shift if surrval.size > (radius * 2 - 1)
      rescue StopIteration
        surrval.shift
      end

      break if surrval.size <= pick
    end
  end

  def revalpha(yielder, enumerator, alpha)
    last_value = nil

    peaks = enumerator&.to_enum
    peaks.each do |value|
      last_value = last_value.nil? ?
        value
        : value * alpha + last_value * (1 - alpha)

      yielder << last_value
    end
  end
end