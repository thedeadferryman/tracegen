require_relative 'signal_scaler'

class NoiseProfile
  class Gaussian
    def initialize(ampl, scaler = nil)
      @scaler = scaler || SignalScaler.new(1)
      @ampl = ampl
    end

    def produce(count)
      orig_count = @scaler.unscale_size count
      original = Enumerator.new(orig_count) { |yi| orig_count.times { yi << rand(-@ampl..@ampl) } }.lazy

      @scaler.scale(original)
    end
  end

  HI_LO_BASE = 1_024

  def initialize(borehole, duration, ampl, hi_low_rate, smoother)
    @borehole = borehole
    @duration = duration
    @ampl = ampl
    @sign_profile = make_sign_profile(hi_low_rate)
    @smoother = smoother
  end

  def produce(count)
    seq = Enumerator.new(count) { |yielder| generate(yielder, count) }.lazy

    if @smoother.nil?
      seq
    else
      @smoother.process(seq)
    end
  end

  private

  def generate(yielder, count)
    data = []
    is_peak = [true, false].sample
    emitted = 0

    loop do
      if data.empty?
        data += is_peak ? single_peak(*peak_params) : single_hole(rand(@borehole))

        is_peak = !is_peak
      end

      unless data.empty?
        yielder << data.shift
        emitted += 1
      end

      break if emitted > count
    end
  end

  def make_sign_profile(hi_lo)
    hi = (HI_LO_BASE * hi_lo).ceil.to_i
    lo = (HI_LO_BASE * (1 - hi_lo)).ceil.to_i

    [
      *[1] * hi,
      *[-1] * lo
    ].shuffle
  end

  def peak_params
    duration = rand(@duration)
    ampl = @sign_profile.sample * rand(@ampl)

    [duration, ampl]
  end

  def single_peak(duration, ampl)
    [ampl] * duration
  end

  def single_hole(duration)
    [0] * duration
  end
end