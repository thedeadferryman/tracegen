# frozen_string_literal: true

require 'time'

class GpxWriter
  def initialize(trkpts, name)
    @trkpts = trkpts
    @name = name
  end

  def write_gpx(file, bar)
    file << write_header
    file << "\n"
    file << write_metadata
    file << "\n"
    file << write_trk_header
    file << "\n"
    write_trkpts(file, bar)
    file << "\n"
    file << write_trk_footer
    file << "\n"
    file << write_footer
    file << "\n"
  end

  def write_header
    %(<?xml version="1.0" encoding="UTF-8"?>
		<gpx
			creator="Huami Amazfit Smart Watch"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xmlns="http://www.topografix.com/GPX/1/1"
			xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3"
			version="1.3">)
  end

  def write_metadata
    %(
	<metadata>
    <name>#{@name}</name>
    <author>Strava Mobile</author>
    <type>Walking</type>
	</metadata>
	)
  end

  def write_trk_header
    %(<trk><trkseg>)
  end

  def write_trk_footer
    %(</trkseg></trk>)
  end

  def write_footer
    %(</gpx>)
  end

  def write_trkpts(file, bar)
    bar.iterate(@trkpts.each) do |trkpt|
      time = trkpt[:time] ? Time.strptime(trkpt[:time].round.to_i.to_s, '%s').iso8601 : nil
      hr = trkpt[:hr].round.to_i
      cad = trkpt[:cad].round.to_i

      file << %(<trkpt lat="#{trkpt[:lat]}" lon="#{trkpt[:lon]}">
				<ele>#{(trkpt[:ele] || 0).ceil(1)}</ele>
				#{time.nil? ? '' : "<time>#{time}</time>"}
				<extensions>
				  <gpxtpx:TrackPointExtension>
					#{hr.nil? ? '' : "<gpxtpx:hr>#{hr}</gpxtpx:hr>"}
      #{cad.nil? ? '' : "<gpxtpx:cad>#{cad}</gpxtpx:cad>"}
				  </gpxtpx:TrackPointExtension>
				</extensions>
			  </trkpt>
        )
    end
  end
end
