require 'csv'
require 'tty-progressbar'

files = (3..5).map { |i| "run_#{i}.csv" }

f = File.open('dataset.csv', 'w')

head = nil

files.map do |file|
	ds = CSV.open(file)

	if head.nil?
		head = ds.shift
		f << CSV.generate_line(head)
	else
		ds.shift
	end

	ds.each do |row|
		f << CSV.generate_line(row)
	end
end

f.close