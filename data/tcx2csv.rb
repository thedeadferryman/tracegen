require 'nokogiri'
require 'date'
require 'csv'
require 'tty-progressbar'

def tcx2csv(ifile, ofile, pbar)
	idata = IO.read(ifile)
	idata.gsub!(%r{(<Time>[\s\S]+?<\/Extensions>)}m, '<Trkpt>\1</Trkpt>') if idata[%r{<Trkpt>}].nil?

	xml = Nokogiri::XML.parse(idata)

	trkpts = []

	xml.css('Activity > Lap > Track > Trkpt').each do |trkpt|
		time = DateTime.parse(trkpt.css('Time').first.text).to_time.to_i
		alt = trkpt.css('AltitudeMeters').first.text.to_f
		dst = trkpt.css('DistanceMeters').first.text.to_f
		hb = trkpt.css('HeartRateBpm > Value').first.text.to_i

		trkpts << {
			time: time,
			alt: alt,
			dst: dst,
			hb: hb,
		}
	end

	start_time = trkpts.first[:time]
	prev_trkpt = nil

	trkpts = trkpts.map do |trkpt|
		ntrkpt = if prev_trkpt.nil?
					 {
						 **trkpt,
						 tdiff: 0,
						 tsdiff: 0,
						 aldiff: 0,
						 dsdiff: 0,
						 phb: 0,
					 }
				 else
					 {
						 **trkpt,
						 tdiff: trkpt[:time] - prev_trkpt[:time],
						 tsdiff: trkpt[:time] - start_time,
						 aldiff: trkpt[:alt] - prev_trkpt[:alt],
						 dsdiff: trkpt[:dst] - prev_trkpt[:dst],
						 phb: prev_trkpt[:hb]
					 }
				 end

		prev_trkpt = ntrkpt
	end.map do |trkpt|
		if trkpt[:phb] <= 0
			nil
		else
			trkpt
		end
	end

	f = File.open(ofile, 'w')

	f << CSV.generate_line(%w[alt dst tdiff tsdiff aldiff dsdiff phb hb])

	pbar.iterate(trkpts.compact.each) do |trkpt|
		f << CSV.generate_line([trkpt[:alt], trkpt[:dst], trkpt[:tdiff], trkpt[:tsdiff],
								trkpt[:aldiff], trkpt[:dsdiff], trkpt[:phb], trkpt[:hb]])
	end

	f.close
end

bars = TTY::ProgressBar::Multi.new('total [[:bar]] :percent', hide_cursor: true, width: 40)

files = (1..5).map do |i|
	ifile = "run_#{i}.tcx"
	ofile = File.basename(ifile, File.extname(ifile)) + '.csv'

	bar = bars.register("#{ifile} -> #{ofile} [[:bar]] :percent")

	[ifile, ofile, bar]
end

files
.map { |v| Thread.new { tcx2csv(*v) } }
.each(&:join)
