require_relative 'src/util/noise_profile'
require_relative 'src/util/signal_scaler'
require_relative 'src/util/signal_smoother'

require 'gruff'

# srand(3)

ssm = SignalSmoother.new(:surlin2, 6)

np = NoiseProfile::Gaussian.new(3, SignalScaler.new([3, 2]))

raw = ssm.process(np.produce(60)).to_a

pp raw.size

ln = Gruff::Line.new

ln.data 'raw', raw.to_a

ln.write