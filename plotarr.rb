require 'gruff'
require 'json'

fn = ARGV.shift
data = JSON.parse IO.read(fn)

ln = Gruff::Line.new

pp data.size

ln.data fn || '', (data.map { |v| v })

ln.write File.basename(fn, File.extname(fn)) + ".png"