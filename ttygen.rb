#!/usr/bin/env ruby
# frozen_string_literal: true

require 'hashie/mash'
require 'tty-progressbar'

require_relative 'src/elevation/elevation_tile_service'
require_relative 'src/track_processor/track_processor'
require_relative 'src/gpx_writer'

ets = ElevationTileService.new(token: 'pk.eyJ1Ijoia21laW5rb3BmIiwiYSI6ImNrcnhobGg4NzBxZ3YydnFuemg2d2dmMTEifQ.fuIlKwPPbL5_DaKWVzY-Vw')

tpu = TrackProcessor.new
                    .with_cart_noise(2.5, 128_000)
                    .with_elevation(ets)
                    .fold
                    .with_metrics
                    .with_avg_speed(rand(1.83..1.9))
                    .rescale(1)
                    .unfold
                    .with_elevation(ets)
                    .fold
                    .with_metrics
                    .unfold(Time.now.to_i)
                    .with_heartbeat_ode(rand(84..97), rand(3.65..3.94))

pregens = Dir['resources/tracemap/_pregen/*.gpx'].map(&File.method(:realpath))
track = load_gpx pregens.sample

segs = tpu.process track

outfn = ARGV.shift || 'generated.gpx'

outfile = File.open(outfn, 'w')

wobar = TrackProcessor.progress('gpx write')

wobar.log "Writing #{outfn}"

GpxWriter.new(segs, 'test').write_gpx(outfile, wobar)

outfile.close
