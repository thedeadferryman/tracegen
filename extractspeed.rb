require 'hashie/mash'
require 'gruff'
require 'tty-progressbar'
require 'date'

require_relative 'src/elevation/elevation_tile_service'
require_relative 'src/track_processor/track_processor'
require_relative 'src/gpx_writer'

def load_gpx(file)
  gpx = Nokogiri::XML(File.open(file))

  track = []

  bar = TrackProcessor.progress 'gpx read'
  bar.log "Selected #{File.basename(file)}"

  prev_trkpt = nil

  bar.iterate(gpx.css('trk trkseg trkpt').each) do |elem|
    # gpx.css('trk trkseg trkpt').each do |elem|
    lat = elem.attributes['lat'].value.to_f
    lon = elem.attributes['lon'].value.to_f
    ele = elem.css('ele').first.text.to_f
    hr = elem.css('extensions')
      &.children
      &.find { |c| c&.name == 'TrackPointExtension' }
      &.children
      &.find { |c| c&.name == 'hr' }
      &.text&.to_f
    cad = elem.css('extensions')
      &.children
      &.find { |c| c&.name == 'TrackPointExtension' }
      &.children
      &.find { |c| c&.name == 'cad' }
      &.text&.to_f

    time = DateTime.parse(elem.css('time').first).to_time.to_i
    vel = 0
    tdiff = 0
    dst = 0

    unless prev_trkpt.nil?
      dst = TrackProcessor.cart_distance({ lat: lat, lon: lon }, prev_trkpt).to_f
      tdiff = (prev_trkpt.time - time).abs

      vel = (dst / [1, tdiff].max).abs
    end

    cad = nil if cad == 0
    hr = nil if hr == 0

    prev_trkpt = Hashie::Mash.new({ lat: lat, lon: lon, ele: ele, tdiff: tdiff, dst: dst, time: time, vel: vel, hr: hr, cad: cad})
    track << prev_trkpt
  end

  track
end

ifn = ARGV.shift
ofn = File.basename(ifn, File.extname(ifn)) + '-speed.json'

track = load_gpx(ifn)

IO.write ofn, JSON.dump(track.map(&:vel).compact)
