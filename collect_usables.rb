# frozen_string_literal: true

require_relative 'src/form_variant'
require_relative 'src/gpx_writer'
require_relative 'src/track_processor/track_processor'
require 'tty-progressbar'

preset = 'Innopolis'

# @param [Array<String>] path
# @param [Array<Hash>] edges
def continue_path(path, edges)
	return [path] if path.last == 'FA'

	nexts = edges
			.select { |edge| edge[:from] == path.last }
			.map { |edge| edge[:to] }

	nexts.reduce([]) do |memo, npt|
		memo + continue_path(path + [npt], edges)
	end
end

def path_variants(current, template)
	return [current] if template.empty?

	template.first[:variants].map do |var|
		path_variants(current + [var], template.slice(1, template.length - 1))
	end.flatten(1)
end

def variant_name(var)
	var.map { |v| v.gsub('/', '.') }.join('_')
end

edges = Dir["#{File.realdirpath("resources/tracemap/#{preset}")}/*"].map do |path|
	from, to = File.basename(path).split('-', 2)

	{
		from: from, to: to,
		path: path, name: File.basename(path)
	}
end

variants = continue_path(['0A'], edges)

edge_variants = variants.map do |variant|
	(0...(variant.size - 1)).map do |i|
		"#{variant[i]}-#{variant[i + 1]}"
	end
end

templates = edge_variants.map do |variant|
	variant.map do |edge|
		{
			edge: edge,
			variants: Dir["#{File.realdirpath("resources/tracemap/#{preset}/#{edge}")}/*"].map do |f|
				"#{edge}/#{File.basename(f, File.extname(f))}"
			end
		}
	end
end

pbar = TTY::ProgressBar::Multi.new(
	"all variants    (:current/:total)  :bar  :percent ETA: :eta",
	bar_format: :blade,
)

TrackProcessor.progress_parent = pbar

tp = TrackProcessor.new
	 .fold
	 .with_metrics
	 .unfold

all_variants = templates.map do |template|
	path_variants([], template)
end.flatten(1).each do |template|
	vf = VariantFormer.new(preset).form(template)
	tp1 = tp.process(vf)

	next if tp1.last.dist < 6_520

	vn = variant_name template

	f = File.open("data/va/#{vn}.gpx", "w")

	GpxWriter.new(vf, vn).write_gpx(f, TrackProcessor.progress("#{vn} gpx"))

	f.close
end