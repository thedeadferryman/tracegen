#include <iostream>
#include "src/Integrator.h"

using namespace std;
using namespace odeint;

int main() {
    StType speeds = { 1.81, 1.81, 1.82, 1.831, 1.8205, 1.8145 };

    auto intg = Integrator(speeds, 0.2);

    cout << intg.calculate(1) << endl;
    cout << intg.calculate(2) << endl;
    cout << intg.calculate(5) << endl;
}