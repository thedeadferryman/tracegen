//
// Created by Дом on 10.08.2021.
//

#ifndef ODEINT_INTEGRATOR_H
#define ODEINT_INTEGRATOR_H

#include "StType.h"
#include "PulserODE.h"
#include <map>

namespace odeint
{
	typedef void (*CallbackT)(double, double);

	class Integrator
	{
	private:
		StType computed;
		PulserODE ode;
		double stepSize;

	public:
		Integrator(const StType &, double, double, double,
				   double, double, double);

		void precompute(double, CallbackT);

		double calculate(double);
	};
}

#endif //ODEINT_INTEGRATOR_H
