//
// Created by Дом on 10.08.2021.
//

#include "Integrator.h"
#include <boost/numeric/odeint.hpp>
#include <algorithm>
#include <iostream>

#define foreach std::for_each
#define fill std::fill

using namespace boost::numeric::odeint;
using namespace odeint;
using StPair = std::pair<const StType &, const double &>;

Integrator::Integrator(
	const StType &speeds,
	double step,
	double npx, double dpx,
	double x0px1, double sp, double xpx1
	) : ode(speeds, npx, dpx, x0px1, sp, xpx1), stepSize(step) {}

void Integrator::precompute(double tMax, CallbackT callback)
{
	auto x = StType{0, 0};

	auto stepper = runge_kutta4<StType>();

	auto begin = make_const_step_time_iterator_begin(stepper, ode, x, 0, tMax / 60.0, stepSize);
	auto end = make_const_step_time_iterator_end(stepper, ode, x);

	computed.clear();
	computed.resize(ceil(tMax * 1.5));
	fill(computed.begin(), computed.end(), -1);

	foreach (begin, end, [this, callback](StPair x)
			 {
				 auto time = x.second * 60.0;

				 callback(time, x.first[0]);

				 auto lower = std::floor(time);
				 auto upper = std::ceil(time);

				 auto lower_i = std::min(static_cast<size_t>(lower), computed.size() - 1);
				 lower_i = std::max(lower_i, static_cast<size_t>(0));
				 auto upper_i = std::min(static_cast<size_t>(upper), computed.size() - 1);
				 upper_i = std::max(upper_i, static_cast<size_t>(0));
				 
				 auto mant = time - lower;

				 if (computed[lower_i] == -1 && computed[upper_i] == -1)
				 {
					 computed[upper_i] = x.first[0];
					 computed[lower_i] = x.first[0];
				 }
				 else if (computed[lower_i] == -1)
				 {
					 computed[lower_i] = x.first[0];
					 computed[upper_i] = computed[upper_i] * (1 - mant) + x.first[0] * mant;
				 }
				 else if (computed[upper_i] == -1)
				 {
					 computed[upper_i] = x.first[0];
					 computed[lower_i] = computed[lower_i] * mant + x.first[0] * (1 - mant);
				 }
				 else
				 {
					 computed[upper_i] = computed[upper_i] * (1 - mant) + x.first[0] * mant;
					 computed[lower_i] = computed[lower_i] * mant + x.first[0] * (1 - mant);
				 }
			 })
		;
}

double Integrator::calculate(double time)
{
	auto lower = std::floor(time);
	auto upper = std::ceil(time);

	auto lower_i = std::min(static_cast<size_t>(lower), computed.size() - 1);
	lower_i = std::max(lower_i, static_cast<size_t>(0));
	auto upper_i = std::min(static_cast<size_t>(upper), computed.size() - 1);
	upper_i = std::max(upper_i, static_cast<size_t>(0));

	auto mant = time - lower;

	return computed[lower_i] * (1 - mant) + computed[upper_i] * mant;
}
