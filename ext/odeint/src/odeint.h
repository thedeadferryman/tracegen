#ifndef ODEINT_ODEINT_H
#define ODEINT_ODEINT_H

#include "macro.h"

#ifdef __cplusplus
#include "Integrator.h"
using namespace odeint;
#else
typedef struct Integrator;
typedef struct StType;
typedef void (*CallbackT)(double, double);
#endif

FFISPEC Integrator *IntegratorNew(StType *, double, double, double,
                                  double, double, double);

FFISPEC void IntegratorPrecompute(Integrator *, double, CallbackT);

FFISPEC double IntegratorCall(Integrator *, double);

FFISPEC void IntegratorDrop(Integrator *);

#endif // ODEINT_ODEINT_H