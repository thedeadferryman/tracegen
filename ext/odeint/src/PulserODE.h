#ifndef ODEINT_PULSERODE_H
#define ODEINT_PULSERODE_H

#include "StType.h"

namespace odeint {
	class PulserODE {
	private:
		StType speeds;
		double npx, dpx, x0px1, sp, xpx1;
	public:
		explicit PulserODE(StType , double, double, double, double, double);

		void operator()(const StType &, StType &, const double);

	private:
		double phi(double) const;

		double speedAt(const double);
	};
}

#endif //ODEINT_PULSERODE_H
