//
// Created by Дом on 10.08.2021.
//

#include "StType.h"
#include <iostream>

using namespace odeint;
using namespace std;

StType *StTypeNew() {
	return new StType();
}

void StTypeAdd(StType *self, double value) {
	try {
	self->push_back(value);
	} catch (std::exception &e) {
		cerr << "Error pushing to StType: " << e.what() << endl;
	}
}

double StTypeAt(StType *self, size_t at) {
	return self->at(at);
}

size_t StTypeSize(StType *self) {
	return self->size();
}

void StTypeDrop(StType *self) {
	delete self;
}
