//
// Created by Дом on 10.08.2021.
//

#ifndef ODEINT_STTYPE_H
#define ODEINT_STTYPE_H

#include <vector>
#include "macro.h"

namespace odeint
{
	using StType = std::vector<double>;
}

FFISPEC odeint::StType *StTypeNew();

FFISPEC void StTypeAdd(odeint::StType *, double);

FFISPEC double StTypeAt(odeint::StType *, std::size_t);

FFISPEC std::size_t StTypeSize(odeint::StType *);

FFISPEC void StTypeDrop(odeint::StType *);

#endif //ODEINT_STTYPE_H
