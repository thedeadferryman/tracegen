#ifndef ODEINT_MACRO_H
#define ODEINT_MACRO_H

#ifdef __declspec
# define FFISPEC extern "C" __declspec(dllexport)
#else
# define FFISPEC extern "C" __attribute__((visibility("default")))
#endif

#endif