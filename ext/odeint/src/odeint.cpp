#include "odeint.h"

using namespace odeint;

Integrator *IntegratorNew(StType *speeds, double stepSize, double npx, double dpx,
                          double x0px1, double sp, double xpx1)
{
    auto sps = StType(*speeds);

    return new Integrator(sps, stepSize, npx, dpx, x0px1, sp, xpx1);
}

void IntegratorPrecompute(Integrator *self, double tMax, CallbackT callback)
{
    self->precompute(tMax, callback);
}

double IntegratorCall(Integrator *self, double d)
{
    return self->calculate(d);
}

void IntegratorDrop(Integrator *self)
{
    delete self;
}