#include "PulserODE.h"

#include <cmath>
#include <utility>

#define pow2(a) ((a) * (a))

using namespace odeint;

PulserODE::PulserODE(StType _speeds, double _npx, double _dpx,
                     double _x0px1, double _sp, double _xpx1) : speeds(std::move(_speeds)),
                                                                npx(_npx), // 2.31e-5
                                                                dpx(_dpx), // 12.81
                                                                x0px1(_x0px1), // 37.12
                                                                sp(_sp), // 0.22
                                                                xpx1(_xpx1) // 2.08e-4
{
}

double PulserODE::phi(double x) const {
	auto num = npx * x;
	auto den = 1 + std::exp(dpx - x);

	return num / den;
}

void PulserODE::operator ()(const StType &x, StType &dx, const double t) {
	auto speed = speedAt(t);

	dx[0] = -x[0] + x0px1 * x[1] + sp * pow2(speed);
	dx[1] = xpx1 * x[1] + phi(x[0]);
}

double PulserODE::speedAt(const double timeMin) {
	auto time = timeMin * 60.0;

	auto lower = std::floor(time);
	auto upper = std::ceil(time);

	if (upper >= static_cast<double>(speeds.size())) {
		if (lower < static_cast<double>(speeds.size())) {
			return speeds[static_cast<size_t>(lower)];
		} else {
			return *speeds.rbegin();
		}
	}

	auto mant = time - lower;

	return (speeds[static_cast<size_t>(lower)] * (1 - mant) + speeds[static_cast<size_t>(upper)] * (mant)) * 3.6; // m/s to km/h
}